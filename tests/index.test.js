
const { retry } = require('../dist');

let i = -1;
const fn = async () => {
  i += 1;
  if (i < 2) {
    throw new Error('Don\'t feel like working');
  }
  return 'Now I work';
};

test('Test succeeds', async () => {
  let res = await retry(fn, { retries: 9 });
  expect(res).toEqual('Now I work');
});

test('Test fails', async () => {
  await expect(retry (() => { throw new Error('no'); }, { wait: 1 }))
    .rejects.toEqual(new Error('no'));
});

jest.setTimeout(20000);

let firstRandom = 0;

test('Test randomness', async () => {
  let retries = 4;
  let before = Date.now();

  await expect(retry (() => { throw new Error('no'); }, { retries }))
    .rejects.toEqual(new Error('no'));

  let after = Date.now();

  // This is going to run `retries` times with 500ms + random amount of time,
  // which means it must take at least t > (`retries - 1` * 500)ms time to run
  expect(after - before).toBeGreaterThan((retries - 1) * 500);
  firstRandom = after - before;
});

test('Test randomness part 2', async () => {
  let retries = 4;
  let before = Date.now();

  await expect(retry (() => { throw new Error('no'); }, { retries }))
    .rejects.toEqual(new Error('no'));

  let after = Date.now();

  // This is going to run `retries` times with 500ms + random amount of time,
  // which means it must take at least t > (`retries - 1` * 500)ms time to run
  expect(after - before).toBeGreaterThan((retries - 1) * 500);
  // This run time can never be the same as the previous
  expect(after - before).not.toBe(firstRandom);
});

test('Test onRetry', async () => {
  const fn = jest.fn();
  try {
    await retry(() => { throw new Error(''); }, { onRetry: fn, wait: 0 });
  }
  catch {}
  expect(fn).toHaveBeenCalledTimes(9);
});

test('Test bail', async () => {
  const fn = jest.fn();
  // This should bail on the first run and never call onRetry
  await expect(retry((bail) => { bail(new Error('')); }, { onRetry: fn, wait: 0 }))
    .rejects.toEqual(new Error(''));
  expect(fn).toHaveBeenCalledTimes(0);
});
