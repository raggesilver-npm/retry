
export type BailFunction = (error: Error) => any;
export type RetryFunction = (bail: BailFunction) => any;

export interface RetryOptions {
  /**
   * Maximum amount of times to retry
   */
  retries: number,
  /**
   * Time (in milliseconds) to wait before retrying
   */
  wait: number,
  /**
   * Wether or not to randomly multiply the wait time with a factor between 1
   * and 2
   */
  randomize: Boolean,
  /**
   * Function executed upon retrial. Receives the error which triggered the
   * retry.
   */
  onRetry: BailFunction,
};

const wait = async (ms: number) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
};

export const retry = async (fn: RetryFunction, opts?: RetryOptions) => {
  let retries: number = opts?.retries || 10; // Can't be 0
  let waitTime: number = (opts?.wait === undefined) ? 500 : opts?.wait;
  let randomize: Boolean = (opts?.randomize === undefined) ? true : !!opts?.randomize;

  let bailed = false;
  let bail = (e: Error) => {
    bailed = true;
    throw e;
  };

  for (let i = 0; i < retries; i++) {
    try {
      let res: any = await fn(bail);
      return (res);
    }
    catch (e) {
      if (i + 1 === retries || bailed) {
        throw e;
      }

      let toWait = waitTime;

      opts?.onRetry?.(e);
      if (randomize) {
        toWait *= (Math.random() + 1);
      }

      await wait(toWait);
    }
  }
};
