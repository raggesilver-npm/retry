module.exports = {
  coverageDirectory: 'coverage',
  rootDir: 'tests/',
  testEnvironment: 'node',
};
